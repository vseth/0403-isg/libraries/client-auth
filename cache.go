package client_auth

import (
	"encoding/json"
	"hash/crc32"
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

// we only cache when we can and ignore errors on reading and writing
// a user might not allow us to write to his home dir etc.

// we add a hash of the request to the token - to make sure the token
// is only used for the same requests
func requestHash(keycloakDomain string, realm string, name string, scopes []string) string {
	type request struct {
		KeycloakDomain string
		Realm          string
		Name           string
		Scopes         []string
	}
	req, _ := json.Marshal(request{
		KeycloakDomain: keycloakDomain,
		Realm:          realm,
		Name:           name,
		Scopes:         scopes,
	})
	return strconv.Itoa(int(crc32.ChecksumIEEE(req)))
}

// readCache reads the cache file and returns the response that was found there. If the token is still
// valid it returns true, otherwise false is returned. The response is still returned as it could
// potentially contain a refresh token
func readCache(hash string) (*Response, bool) {
	b, err := readCacheFile(hash)
	if err != nil {
		return nil, false
	}
	r := &Response{}
	json.Unmarshal(b, r)
	// check if the token is stil valid
	exp, ok := r.Claims["exp"].(float64)
	if !ok {
		return nil, false
	}
	// we check if the token is stil valid in 10 seconds, the 10 seconds
	// are for a better UX incase the token is just about to expire
	if !time.Now().Add(time.Second * 10).Before(time.Unix(int64(exp), 0)) {
		return r, false
	}
	return r, true
}

func writeCache(hash string, r Response) {
	body, _ := json.Marshal(r)
	writeCacheFile(hash, body)
}

func readCacheFile(name string) ([]byte, error) {
	return ioutil.ReadFile(absFilePath(name))
}

func writeCacheFile(name string, body []byte) error {
	// create cache dirt if not exists
	if _, err := os.Stat(absCacheDirPath()); os.IsNotExist((err)) {
		if err := os.MkdirAll(absCacheDirPath(), 0755); err != nil {
			return err
		}
	} else if err != nil {
		return err
	}
	return ioutil.WriteFile(absFilePath(name), body, 0644)
}

func absFilePath(name string) string {
	return filepath.Join(absCacheDirPath(), name)
}

func absCacheDirPath() string {
	home, _ := os.UserHomeDir()
	return filepath.Join(home, pathCacheDir)
}
