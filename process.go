package client_auth

import (
	"fmt"
	"strconv"
	"time"

	"github.com/pkg/errors"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/lestrrat/go-jwx/jwk"
)

type keycloakImplicitResponse struct {
	Token     string `json:"access_token"`
	IDToken   string `json:"id_token"`
	State     string `json:"session_state"`
	TType     string `json:"token_type"`
	ExpiresIn string `json:"expires_in"`

	Error string `json:"error_description"`
}

const (
	msgUnexpectedTokenFormat = "unexpected token format"
)

func processImplicitResponse(resp keycloakImplicitResponse, jwks *jwk.Set, nonce string, scopes []string) (*Response, error) {
	// check if there is an error
	if resp.Error != "" {
		return nil, fmt.Errorf("keycloak returned with an error:\n%s", resp.Error)
	}

	// parse ID token
	claims, err := parseClaimsFromToken(resp.IDToken, jwks)
	if err != nil {
		return nil, err
	}

	if tokenNonce, ok := claims["nonce"].(string); !ok {
		return nil, errors.New(msgUnexpectedTokenFormat)
	} else if tokenNonce != nonce {
		return nil, errors.New("invalid nonce")
	}

	t, err := strconv.Atoi(resp.ExpiresIn)
	if err != nil {
		return nil, fmt.Errorf("error parsing expires_in of keycloak response: %s", err)
	}
	expirationTime := time.Now().Add(time.Duration(t) * time.Minute)

	return &Response{
		AccessToken:         resp.Token,
		TokenExpirationTime: expirationTime,
		IDToken:             resp.IDToken,
		Claims:              claims,
		Scopes:              scopes,
	}, nil
}

type keycloakCodeResponse struct {
	Token            string `json:"access_token"`
	IDToken          string `json:"id_token"`
	State            string `json:"session_state"`
	ExpiresIn        int    `json:"expires_in"`
	RefreshExpiresIn int    `json:"refresh_expires_in"`
	RefreshToken     string `json:"refresh_token"`
	TType            string `json:"token_type"`
	NotBeforePolicy  int    `json:"not-before-policy"`
	Scope            string `json:"scope"`

	Error string `json:"error_description"`
}

func processCodeResponse(resp keycloakCodeResponse, jwks *jwk.Set, nonce string, scopes []string) (*Response, error) {
	// check if there is an error
	if resp.Error != "" {
		return nil, fmt.Errorf("keycloak returned with an error: %s", resp.Error)
	}

	// parse ID token
	claims, err := parseClaimsFromToken(resp.IDToken, jwks)
	if err != nil {
		return nil, errors.Wrap(err, "failed parsing id-token")
	}

	if tokenNonce, ok := claims["nonce"].(string); !ok {
		return nil, errors.New(msgUnexpectedTokenFormat)
	} else if tokenNonce != nonce {
		return nil, errors.New("invalid nonce")
	}

	t := time.Now()
	expirationTime := t.Add(time.Duration(resp.ExpiresIn) * time.Minute)
	refreshExpirationTime := t.Add(time.Duration(resp.RefreshExpiresIn) * time.Minute)

	return &Response{
		AccessToken:           resp.Token,
		TokenExpirationTime:   expirationTime,
		RefreshToken:          resp.RefreshToken,
		RefreshExpirationTime: refreshExpirationTime,
		IDToken:               resp.IDToken,
		Claims:                claims,
		Scopes:                scopes,
	}, nil
}

func parseClaimsFromToken(token string, jwks *jwk.Set) (jwt.MapClaims, error) {
	// parse token
	parsedToken, err := jwt.Parse(token, func(t *jwt.Token) (interface{}, error) {
		kid, ok := t.Header["kid"].(string)
		if !ok {
			return nil, errors.New("expected kid in jwt header")
		}
		if res := jwks.LookupKeyID(kid); 0 < len(res) {
			return res[0].Materialize()
		}
		return nil, errors.New("no key for kid")
	})
	// ignore "Token used before issued" error, as client and server are not perfectly in sync
	if err != nil && err.Error() != "Token used before issued" {
		return nil, err
	}

	jwtClaims, ok := parsedToken.Claims.(jwt.MapClaims)
	if !ok {
		return nil, errors.New(msgUnexpectedTokenFormat)
	}

	return jwtClaims, nil
}
