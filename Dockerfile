ARG GOLANG_IMAGE=golang:1.13

FROM ${GOLANG_IMAGE}
WORKDIR /workdir
COPY go.mod .
RUN go mod download
COPY . .
CMD go build -o $OUTPUT .
