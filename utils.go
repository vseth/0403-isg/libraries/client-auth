package client_auth

import (
	"crypto/rand"
	"crypto/sha256"
	"encoding/base64"
	"errors"
	"fmt"
	"net"
	"net/url"
	"os/exec"
	"runtime"
	"strings"

	"github.com/lestrrat/go-jwx/jwk"
)

func openURL(url string) error {
	switch runtime.GOOS {
	case "darwin":
		return exec.Command("open", url).Start()
	case "linux", "freebsd":
		if _, err := exec.LookPath("xdg-open"); err != nil {
			// Linux does not always have xdg-open (e.g. in Windows Subsystem for Linux)
			fmt.Printf("Could not launch browser: xdg-open is not present in PATH\n")
			return nil
		}
		return exec.Command("xdg-open", url).Start()
	case "windows":
		return exec.Command("rundll32", "url.dll,FileProtocolHandler", url).Start()
	default:
		return fmt.Errorf("%s not supported", runtime.GOOS)
	}
}

func listenerForFirstFreePort(ports ...int) (ln net.Listener, err error) {
	if len(ports) < 1 {
		err = errors.New("no port provided")
		return
	}
	for _, p := range ports {
		addr := fmt.Sprintf("localhost:%v", p)
		ln, err = net.Listen("tcp", addr)
		if err == nil {
			return
		}
	}
	return
}

func setURLParams(url string, params map[string]string) string {
	var buf strings.Builder
	for k, v := range params {
		if buf.Len() == 0 {
			buf.WriteByte('?')
		} else {
			buf.WriteByte('&')
		}
		buf.WriteString(k)
		buf.WriteByte('=')
		buf.WriteString(v)
	}
	return url + buf.String()
}

func generateNonce() (string, error) {
	const nonceLength = 32
	b := make([]byte, nonceLength)
	_, err := rand.Read(b)
	return base64.URLEncoding.EncodeToString(b), err
}

func secureDomain(rawurl string, httpOnly bool) (string, error) {
	preSecure := "https://"
	preInsecure := "http://"
	if !httpOnly && !strings.HasPrefix(rawurl, preSecure) {
		rawurl = strings.TrimPrefix(rawurl, preInsecure)
		rawurl = preSecure + rawurl
	}
	if httpOnly && !strings.HasPrefix(rawurl, preInsecure) {
		rawurl = preInsecure + rawurl
	}
	url, err := url.Parse(rawurl)
	if err != nil {
		return "", err
	}
	if !strings.HasPrefix(url.Host, "localhost") && !strings.HasPrefix(preSecure, url.Scheme) {
		return "", errors.New("insecure (http) can only be used on localhost")
	}
	return rawurl, nil
}

func getKeycloakJwks(domain string, realm string) (*jwk.Set, error) {
	keycloakCertURL := fmt.Sprintf(domain+keycloakBasePath+"/certs", realm)
	return jwk.FetchHTTP(keycloakCertURL)
}

// Creates a high-entropy cryptographic random string as per RFC 7636 4.1. Internally it uses a
// 32-octet sequence from the `crypto/rand` PRNG
func createVerifier() (string, error) {
	r := make([]byte, 32)
	_, err := rand.Read(r)
	if err != nil {
		return "", err
	}
	return base64URLEncode(r), nil
}

// Creates a code challenge derived from the code verifier using the "S256" code challenge method.
// 	code_challenge = BASE64URL-ENCODE(SHA256(ASCII(code_verifier)))
func createChallenge(verifier string) string {
	h := sha256.New()
	h.Write([]byte(verifier))
	sum := h.Sum(nil)
	return base64URLEncode(sum)
}

// Implements a base64urlencode function as defined in RFC 7636 Appendix A.
func base64URLEncode(data []byte) string {
	encoding := base64.URLEncoding.WithPadding(base64.NoPadding)
	return encoding.EncodeToString(data)
}

// ensureOpenIdScopeIsPresent ensures that the "openid" scope is present in the scope slice as required by the openid specification
func ensureOpenIdScopeIsPresent(scopes []string) []string {
	containsOpenidScope := false
	for _, scope := range scopes {
		if scope == "openid" {
			containsOpenidScope = true;
			break;
		}
	}
	if !containsOpenidScope {
		scopes = append(scopes, "openid")
	}
	return scopes
}


