package client_auth

const html = `
<!DOCTYPE html>
    <html>
        <div id="message">one sec...</div>
    <script>
        // read hash values
        var values = {}
        window.location.hash.substring(1).split('&').forEach(function(param) {
            var keyval = param.split('=');
            values[keyval[0]] = keyval[1];
        });
        // send them back to the server
        var xhr = new XMLHttpRequest();
        var url = window.location.href;
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(values));
        if (values.error != undefined) {
            document.getElementById("message").innerHTML = "<h2>Error: " + values.error + "</h2>" + values.error_description;
        } else {
            // display success message
            document.getElementById("message").innerHTML = "<h2>Authentication successful!</h2>Return back to your application.<br>This tab can be closed.";
        }
        xhr.onreadystatechange = function () {
            open(location, '_self').close();
        };
    </script>
</html>`
