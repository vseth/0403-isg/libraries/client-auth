# client-auth

Go auth package for client applications. [go.getsip.ethz.ch/client-auth](https://go.getsip.ethz.ch/cli-auth)

## Usage

The binary wrapper in [cli-auth2](https://gitlab.ethz.ch/vseth/0403-isg/cli/cli-auth2) gives a good example on how to use the package.

## How it works

Cli-auth first fetches the certificate from the server and generates a random nonce. Then it spawns a webserver locally which it uses to handle the keycloak callback (multiple ports should be provided to avoid conflicts with ports already in use locally). Then the keycloak URL is opened for the user to authenticate. If the user authenticates before a timeout occurs keycloak will redirect to the local callback server with the session token as a fragment in the URL. The callback server servers a javascript program to extract the session token from the fragment and send it to the server as a POST request. The server reads the session token and returns it. The `id_token` is then validated using the certificate and the nonce.
