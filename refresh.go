package client_auth

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

func (r *Response) refreshTokenExpired(leeway time.Duration) bool {
	return time.Now().Add(leeway).After(r.RefreshExpirationTime)
}

// Refresh tries to regresh the response passed as a parameter and returns a new response
// if it was successful - only works when a refresh_token is present
func (r *Response) Refresh(keycloakDomain, realm, name, clientID string) (*Response, error) {
	if r.RefreshToken == "" {
		return nil, errors.Errorf("no refresh token found")
	}

	if r.refreshTokenExpired(time.Duration(10) * time.Second) {
		return nil, errors.Errorf("refresh token expired")
	}

	data := url.Values{
		"grant_type":    {"refresh_token"},
		"refresh_token": {r.RefreshToken},
	}
	if clientID != "" {
		data["client_id"] = []string{clientID}
	}

	url := keycloakDomain + fmt.Sprintf(keycloakBasePath, realm) + pathToken

	payload := strings.NewReader(data.Encode())
	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		return nil, errors.Wrap(err, "refresh request failed")
	}

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	res, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, errors.Wrap(err, "refresh request failed")
	}

	rsp := keycloakCodeResponse{}
	err = json.NewDecoder(res.Body).Decode(&rsp)
	if err != nil {
		return nil, err
	}

	jwks, err := getKeycloakJwks(keycloakDomain, realm)
	if err != nil {
		return nil, err
	}

	tokenNonce, ok := r.Claims["nonce"].(string)
	if !ok {
		return nil, errors.New(msgUnexpectedTokenFormat)
	}

	resp, err := processCodeResponse(rsp, jwks, tokenNonce, r.Scopes)
	if err != nil {
		return nil, err
	}
	hash := requestHash(keycloakDomain, realm, name, r.Scopes)
	writeCache(hash, *resp)

	return resp, nil
}
