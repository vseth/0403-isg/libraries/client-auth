package client_auth

import (
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"strings"
	"time"

	"github.com/pkg/errors"
)

// Flow represents an OpenID Connect flow
type Flow int

const (
	// ImplicitFlow represents the OAuth 2.0 flow in which all tokens are returned from the Authorization Endpoint
	// and neither the Token Endpoint nor an Authorization Code are used.
	ImplicitFlow Flow = iota
	// PKCECodeFlow represents the OAuth 2.0 flow in which an Authorization Code is returned from the
	// Authorization Endpoint and all tokens are returned from the Token Endpoint. This flow represents
	// the PKCE variant as specified in rfc7636. (Proof Key for Code Exchange by OAuth Public Clients)
	PKCECodeFlow

	// DefaultKeycloakDomain is the keycloakDomain which is used when none is specified
	DefaultKeycloakDomain = "https://auth.vseth.ethz.ch"
	// DefaultRealm is the realm that is used when none is specified
	DefaultRealm = "VSETH"

	implicitResponseType = "token+id_token"
	codeResponseType     = "code"

	keycloakBasePath = "/auth/realms/%s/protocol/openid-connect"
	pathToken        = "/token"
	pathCallback     = "/callback"
	pathCacheDir     = ".config/vseth/cli-auth"
	timeout          = time.Minute * 3
)

// Response contains all the data that cli-auth was able to gather. Depending on the flow a refresh-token
// may or may not be set. If set it can be used to get a new response using the `Refresh` method.
type Response struct {
	RefreshToken          string                 `json:"refresh_token,omitempty"`
	RefreshExpirationTime time.Time              `json:"refresh_expiration_time,omitempty"`
	AccessToken           string                 `json:"access_token,omitempty"`
	TokenExpirationTime   time.Time              `json:"expiration_time,omitempty"`
	IDToken               string                 `json:"id_token,omitempty"`
	Claims                map[string]interface{} `json:"claims"`
	Scopes                []string               `json:"scopes"`
}

// Client is an authenticator that is thought for applications that allow users to interact with a browser
type Client struct {
	Flow  Flow // default: ImplicitFlow
	Name  string
	Ports []int // localports to run the callback server

	KeycloakDomain string // default: DefaultKeycloakDomain
	Realm          string // default: DefaultRealm

	DisableDialog bool
	Insecure      bool
}

// Authenticate the client - using either OIDC implicit flow or PKCE Code flow. This will start a webserver
// and open a browser. It should only be used for direct user authentication. Not for scripts and service accounts.
func (c *Client) Authenticate(scopes ...string) (resp *Response, err error) {
	// set empty parameters to default (for usage as a go package)
	if c.KeycloakDomain == "" {
		c.KeycloakDomain = DefaultKeycloakDomain
	}
	if c.Realm == "" {
		c.Realm = DefaultRealm
	}

	c.KeycloakDomain, err = secureDomain(c.KeycloakDomain, c.Insecure)
	if err != nil {
		return nil, err
	}

	// ensure that the openid scope is present
	scopes = ensureOpenIdScopeIsPresent(scopes)

	// check if the token is already cached
	hash := requestHash(c.KeycloakDomain, c.Realm, c.Name, scopes)
	resp, ok := readCache(hash)
	if ok {
		return resp, nil
	}
	if resp != nil {
		r, err := resp.Refresh(c.KeycloakDomain, c.Realm, c.Name, c.Name)
		if err == nil {
			return r, nil
		}
	}

	// load the certs to verify the token
	jwks, err := getKeycloakJwks(c.KeycloakDomain, c.Realm)
	if err != nil {
		return nil, err
	}

	// If code flow is used this channel will be populated with the code once received
	codeRspChan := make(chan string)
	// If implicit flow is used this channel will receive the JSON response
	implicitRspChan := make(chan keycloakImplicitResponse)

	errChan := make(chan error)

	keycloakAuthURL := fmt.Sprintf(c.KeycloakDomain+keycloakBasePath+"/auth", c.Realm)
	if strings.Contains(keycloakAuthURL, "%!") {
		return nil, errors.New("invalid keycloakDomain cannot contain any % signs")
	}

	mux := http.NewServeMux()
	// The handle function sends a javascript program that reads the params from
	// the anchor part of the URL and sends them again as json a POST request.
	// We do this because sending it directly as a form from keycloak triggers
	// a warning in browser (sending form over insecure connection). The anchor
	// part has to be read by javascript as it is not sent to the server.
	mux.HandleFunc(pathCallback, func(w http.ResponseWriter, r *http.Request) {
		switch r.Method {
		case "GET":
			w.Write([]byte(html))
		case "POST":
			w.Write([]byte(""))

			if c.Flow == ImplicitFlow {
				var s keycloakImplicitResponse

				err := json.NewDecoder(r.Body).Decode(&s)
				if err != nil {
					errChan <- errors.New("error decoding keycloak response: " + err.Error())
				} else if s.Error != "" {
					errChan <- errors.New("keycloak error: " + s.Error)
				} else {
					implicitRspChan <- s
				}
			} else if c.Flow == PKCECodeFlow {
				ref := r.Header.Get("Referer")
				refURL, err := url.Parse(ref)
				if err != nil {
					errChan <- errors.New("error decoding referer url: " + err.Error())
					return
				}
				code := refURL.Query().Get("code")
				codeRspChan <- code
			}
		}
	})

	type tcpKeepAliveListener struct {
		*net.TCPListener
	}
	ln, err := listenerForFirstFreePort(c.Ports...)
	if err != nil {
		return resp, err
	}

	callbackHost := ln.Addr().String()
	srv := &http.Server{Addr: callbackHost, Handler: mux}
	go func() {
		err := srv.Serve(tcpKeepAliveListener{ln.(*net.TCPListener)})
		// Wait for server to exit and close channels
		if err != nil && err != http.ErrServerClosed {
			errChan <- err
		}
		close(codeRspChan)
		close(implicitRspChan)
		close(errChan)
	}()

	callbackHost = strings.Replace(callbackHost, "127.0.0.1", "http://localhost", 1)
	var urlParams map[string]string

	// Our PKCE verifier string - we need it later to get our token
	var verifier string

	nonce, err := generateNonce()
	if err != nil {
		return nil, err
	}

	if c.Flow == ImplicitFlow {
		urlParams = map[string]string{
			"response_type": implicitResponseType,

			"client_id":    c.Name,
			"scope":        strings.Join(scopes, "+"),
			"redirect_uri": callbackHost + pathCallback,
			"nonce":        nonce,
		}
	} else if c.Flow == PKCECodeFlow {
		verifier, err = createVerifier()
		if err != nil {
			return nil, err
		}

		challenge := createChallenge(verifier)
		fmt.Printf("%s\n", callbackHost+pathCallback)
		urlParams = map[string]string{
			"response_type": codeResponseType,

			"code_challenge":        challenge, // Our PKCE code challenge
			"code_challenge_method": "S256",    // Defaults to "plain", thus we explicity have to set it

			"client_id":    c.Name,
			"scope":        strings.Join(scopes, "+"),
			"redirect_uri": callbackHost + pathCallback,
			"nonce":        nonce,
		}
	}

	authURL := setURLParams(keycloakAuthURL, urlParams)
	if err := openURL(authURL); err != nil {
		return resp, fmt.Errorf("failed to open authentication URL:\n%s", err.Error())
	}

	if !c.DisableDialog {
		fmt.Printf("%s\nGo to your browser for authentication.\n", authURL)
	}

	// Wait for one of the following
	select {
	case code := <-codeRspChan:
		err = srv.Shutdown(context.Background())
		if err != nil {
			return nil, fmt.Errorf("error shuting down http server:\n%s", err.Error())
		}

		data := url.Values{
			"grant_type":    {"authorization_code"},
			"code_verifier": {verifier},
			"code":          {code},
			"client_id":     {c.Name},
			"redirect_uri":  {callbackHost + pathCallback},
			"response_type": {"id_token"},
		}

		url := c.KeycloakDomain + fmt.Sprintf(keycloakBasePath, c.Realm) + pathToken

		payload := strings.NewReader(data.Encode())
		req, err := http.NewRequest("POST", url, payload)
		if err != nil {
			return nil, errors.Wrap(err, "token request failed")
		}

		req.Header.Add("content-type", "application/x-www-form-urlencoded")
		res, err := http.DefaultClient.Do(req)
		if err != nil {
			return nil, errors.Wrap(err, "token request failed")
		}

		rsp := keycloakCodeResponse{}
		err = json.NewDecoder(res.Body).Decode(&rsp)
		if err != nil {
			return nil, errors.Wrap(err, "decoding token endpoint response failed")
		}

		// Check whether an error was sent from the http server while we
		// were sending the token request
		select {
		case err := <-errChan:
			if err != nil {
				return nil, err
			}
		default:
			// Do nothing
		}

		resp, err := processCodeResponse(rsp, jwks, nonce, scopes)
		if err != nil {
			return nil, err
		}
		writeCache(hash, *resp)

		// Block on errChan until the http server is shut down
		return resp, <-errChan
	case rsp := <-implicitRspChan:
		err = srv.Shutdown(context.Background())
		if err != nil {
			return nil, fmt.Errorf("error shuting down http server:\n%s", err.Error())
		}

		resp, err := processImplicitResponse(rsp, jwks, nonce, scopes)
		if err != nil {
			return nil, err
		}

		writeCache(hash, *resp)

		// Block on errChan until the http server is shut down
		return resp, <-errChan
	case err = <-errChan:
		shutdownErr := srv.Shutdown(context.Background())
		if shutdownErr != nil {
			return nil, fmt.Errorf("error shuting down http server:\n%s", shutdownErr.Error())
		}
		// We got an error. Something is pretty wrong. Propagate the error
		return resp, fmt.Errorf("%s", err.Error())
	case <-time.After(timeout):
		err = srv.Shutdown(context.Background())
		if err != nil {
			return nil, fmt.Errorf("error shuting down http server:\n%s", err.Error())
		}
		// We have a timeout. Let's return a timeout error after trying to close the server
		err = <-errChan
		if err != nil {
			return resp, fmt.Errorf("error while shutting down http server after timeout:\n%s", err.Error())
		}
		return resp, fmt.Errorf("timeout while authorizing")
	}
}

// ServiceAccountClient implements the same authenticator interface as 'Client' but uses the Client Credentials Grant
type ServiceAccountClient struct {
	Username string // Username of SA
	Password string // Password of SA

	KeycloakDomain string // default: DefaultKeycloakDomain
	Realm          string // default: DefaultRealm

	Insecure bool
}

// Authenticate the service account using the Client Credentials Grant
// Note: As of writing this. Keycloak does not seem to support requesting certain scopes and will return
// all roles regardless of the requested scopes.
func (sa *ServiceAccountClient) Authenticate(scopes ...string) (*Response, error) {
	// set empty parameters to default (for usage as a go package)
	if sa.KeycloakDomain == "" {
		sa.KeycloakDomain = DefaultKeycloakDomain
	}
	if sa.Realm == "" {
		sa.Realm = DefaultRealm
	}

	var err error
	sa.KeycloakDomain, err = secureDomain(sa.KeycloakDomain, sa.Insecure)
	if err != nil {
		return nil, err
	}

	// ensure that the openid scope is present
	scopes = ensureOpenIdScopeIsPresent(scopes)

	// check if the token is already cached
	hash := requestHash(sa.KeycloakDomain, sa.Realm, sa.Username, scopes)
	if resp, ok := readCache(hash); ok {
		return resp, nil
	}

	//load the certs to verify the token
	jwks, err := getKeycloakJwks(sa.KeycloakDomain, sa.Realm)
	if err != nil {
		return nil, err
	}

	keycloakTokenURL := fmt.Sprintf(sa.KeycloakDomain+keycloakBasePath+"/token", sa.Realm)
	if strings.Contains(keycloakTokenURL, "%!") {
		return nil, errors.New("invalid keycloakDomain cannot contain any % signs")
	}

	c := &http.Client{}
	postValues := url.Values{"grant_type": {"client_credentials"}, "scope": {strings.Join(scopes, " ")}}
	req, err := http.NewRequest(http.MethodPost, keycloakTokenURL, strings.NewReader(postValues.Encode()))
	if err != nil {
		return nil, fmt.Errorf("error building request for service account\n %w", err)
	}
	req.SetBasicAuth(sa.Username, sa.Password)
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	r, err := c.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error requesting token for service account\n %w", err)
	}
	defer r.Body.Close()

	raw, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading response for service account\n %w", err)
	}

	resp := &Response{}
	err = json.Unmarshal(raw, resp)
	if err != nil {
		return resp, fmt.Errorf("error unmarshaling response for service account\n %w", err)
	}
	if resp.AccessToken == "" {
		return resp, fmt.Errorf("unexpected response from keycloak: %v", string(raw))
	}

	// for the service account we do not get an id token, so we have to parse the
	// access token here to get the claims
	claims, err := parseClaimsFromToken(resp.AccessToken, jwks)
	if err != nil {
		return nil, err
	}
	resp.Claims = claims

	// update the cache
	writeCache(hash, *resp)

	return resp, nil
}
