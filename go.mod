module go.getsip.ethz.ch/client-auth

go 1.12

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/lestrrat/go-jwx v0.0.0-20210302221443-a9d01c1b7121
	github.com/lestrrat/go-pdebug v0.0.0-20180220043741-569c97477ae8 // indirect
	github.com/pkg/errors v0.9.1
	github.com/stretchr/testify v1.7.0 // indirect
)
